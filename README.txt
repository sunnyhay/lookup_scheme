1. prefix and trace files come from routeview and potaroo projects.
   
2. run these three schemes with both prefix file and trace file as two arguments.
   schemev2.cpp slightly modifies the lookup code. Each packet address is now stored
in a vector for lookup. The searching time is slightly improved from 4.57��s VS 4.90��s
to the first version of our scheme using 131702 routing table and trace files.

3. ten prefix files and five trace files.
   for our scheme and TBMP, five prefix files are
     addnextHop-ipv4-as131072.txt
     addnextHop-ipv4-asorigin-05312013.txt
     addnextHop-ipv4-asoriginmax-05312013.txt
     addnextHop-ipv4-aspath-05312013.txt
     addnextHop-ipv4-asppath-05312013.txt
   for BSOL, these file prefix files are ordered by prefix length due to the requirement
of marker and BMP precomputation. 
     as131072-ipv4-prefixordered.txt
     as6447-asorigin-ipv4-prefixordered.txt
     as6447-asoriginmax-ipv4-prefixordered.txt
     as6447-aspath-ipv4-prefixordered.txt
     as6447-asppath-ipv4-prefixordered.txt
    five trace files are
     traceIPv4-routeview-as6447-1.txt
     traceIPv4-routeview-as6447-2.txt
     traceIPv4-routeview-as6447-3.txt
     traceIPv4-routeview-as131072-1.txt
     traceIPv4-routeview-as131072-2.txt     

4. how to watch the performance?
   for search time and preprocess time, version 1 of our scheme directly includes
parsing procedure from line of prefix(trace) file to IP address array.
   the average search time is total search time over packet number.
   the average preprocess time is total preprocess time over prefix number.
   for memory usage, in our scheme is 
     �� [number of entries * (sizeof(key) + sizeof(bnode))] 
     e.g. for AS131072 routing table is
        142350 * (8 + 64)   //in 24-bit hash table
      + 21330 * (8 + 64)    //in 16-bit hash table
      + 184 * (8 + 64)        //in 8-bit hash table
      = 10249200 + 1535760 + 13248
      = 13205716 bytes = 11.3 Mb

   In TBMP implementation, we have one 13-bit root node and numerous children nodes. 
Total 190381 child nodes exist under root. Each child node (tnode) consists of four
parts: 16-bit internal bitmap, 16-bit external bitmap, nexthop array and children 
array. Each tnode has size of bytes: 64. The root node (rnode) has size of bytes: 4144.
The total prefix number in 131072 is 351899. So there are 351899 integer numbers to be
counted. For 131072 routing table, the total memory allocation is
   4144 + 190381 * 64 + 351899 * 4 = 4144 + 12184384 + 1407596 = 13.0 MB

   In BSOL implementation, we have the uniform nodes over all hash tables. Each hash 
value is a pair of <string, node>. Each node consists of four parts: string bmp, int 
mask, int nexthop and int flag. The node��s size is 24 bytes. The total node amount in 
131072 is 464407. Among them there are 112509 pure marker nodes, 341628 real prefix 
nodes and 10270 hybrid nodes. For 131072 routing table, the total memory allocation is
   (24+8) * 464407 = 14861024 = 14.2 MB


     